# Running Gitlab EE Locally
Using Docker compose, this will install and configure Gitlab EE. The installation of Gitlab EE will include a configured Gitlab Runner and an external Registry for container images.

TODO: The Container Registry is external to the project because I didn't generate certificates to trust.

TODO: Use Registry built into Gitlab EE, create certificate trust, etc. 


## Prerequisites

1. If required, Gitlab EE License file
1. Install [Docker Desktop](https://www.docker.com/products/docker-desktop)
1. Recommended: Increase resources for Docker for Desktop (Preferences -> Advanced). Increase CPU and Memory

## Running Gitlab
1. Clone repository
1. In the root of the repository run the Docker compose command
    1. `$ docker-compose up -d`
    1. Tail the startup logs `$ docker-compose logs -f`
1. Open a browser to [http://localhost](http://localhost)
1. Configure Gitlab

## Adding Gitlab Runners
1. Log into Gitlab UI
1. In the Admin screens, get the Gitlab Runner registration token
1. Set `PROJECT_REGISTRATION_TOKEN` token: `$ export PROJECT_REGISTRATION_TOKEN=<TOKEN_VAULE>`
1. Register the runner with Gitlab. Run the command:
```
docker exec -d docker_runner_1 gitlab-runner register \
  --non-interactive \
  --executor "docker" \
  --docker-image "docker:19.03.1" \
  --docker-volumes /var/run/docker.sock:/var/run/docker.sock \
  --docker-volume-driver overlay2 \
  --url "http://web" \
  --clone-url "http://host.docker.internal" \
  --registration-token $PROJECT_REGISTRATION_TOKEN \
  --description "docker-runner" \
  --tag-list "docker" \
  --run-untagged="true" \
  --locked="false" \
  --description "Amazing Docker Runner" \
  --access-level="not_protected"
```
5. Refresh the Runners screen. You should now see an available runner.